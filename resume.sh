#!/bin/sh

if [ -x "$(command -v yarn)" ]; then
    # yarn is installed
    yarn run start $@
elif [ -x "$(command -v npm)" ]; then
    # npm is installed
    npm run start $@
else
    echo "npm and yarn are not installed."
fi
# 📜 resume.ts

resume.ts is a simple typescript project to TeX create a resume from a template.
You are bored of creating a new resume ? With resume.ts you just have to write a template in TeX, a ``JSON`` / ``TOML`` / ``YAML`` file, and you resume will be compiled !

:bookmark_tabs: Table of contents
=================
- [resume.ts](#resume.ts)
- [Prerequisites](#prerequisites)
- [Getting started](#getting-started)
- [Resume config](#resume-config)
- [Compilers](#compilers)
- [Templates](#templates)
- [Dependencies](#dependencies)
  - [`dependencies`](#depencies-1)
  - [`devDependencies`](#devdependencies)


# :gear: Prerequisites

To build and run this app locally you will need:
- Install [Node.js](https://nodejs.org/en/)
- Install [npm](https://www.npmjs.com/get-npm) or [Yarn](https://classic.yarnpkg.com/en/docs/install/#debian-stable)

- One of listed [compilers](#compilers)

# :arrow_forward: Getting started

- Clone this repository
```shell
git clone https://gitlab.com/Zarato/resume.ts
```
- Install dependencies
```shell
cd resume.ts
# if you are using npm
npm install
# if you are using yarn
yarn add
```

- Create a template
```shell
# if you are using npm
npm run start new template
# if you are using yarn
yarn run start new template
```
this will create a template with only a header. To create a complete template, check [this](#templates)

- Create a new resume
```shell
# if you are using npm
npm run start new
# if you are using yarn
yarn run start new
```
and follow the guide. For more configuration on how to config the resume, please check [this part](#resume-config)

- Compile a resume
```shell
# if you are using npm
npm run start compile <resume.json/toml/yaml>
# if you are using yarn
yarn run start compile <resume.json/toml/yaml>
```

# :wrench: Resume config
TODO

# :sparkle: Compilers

- [pdflatex](https://www.latex-project.org/get/)

# :page_with_curl: Templates
TODO

# :file_cabinet: Dependencies

## `dependencies`

| Package                         | Description             |
| ------------------------------- | ----------------------- |
| [tslint-config-airbnb](https://www.npmjs.com/package/tslint-config-airbnb)     | A TSLint config for Airbnb JavaScript Style Guide |

## `devDependencies`
| Package                         | Description             |
| ------------------------------- | ----------------------- |
| [jest](https://jestjs.io/)      | Testing library for JavaScript |
| child_process                   | Run shell commands |
| [ts-jest](https://github.com/kulshekhar/ts-jest) | Support for Jest to test projects written in Typescript |
| [tslint](https://palantir.github.io/tslint/) | Linter for TypeScript |
| [typescript](https://www.typescriptlang.org/) | TypeScript compiler/type checker |
// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html

module.exports = {
    transform: {
        '\\.(ts)$': 'ts-jest',
    },
    testPathIgnorePatterns: ['/build/', '/node_modules/'],
    verbose: true,
    testEnvironment: 'node',
    globals: {
        'ts-jest': {
            isolatedModules: true,
        },
    },
};

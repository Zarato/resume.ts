import { ConfigModel } from '../model';
import * as fs from 'fs';
import YAML from 'yaml';

export async function fromYaml(filename: string): Promise<ConfigModel> {
    const file = await fs.promises.readFile(filename, { encoding: 'utf-8' });
    return <ConfigModel>YAML.parse(file);
}
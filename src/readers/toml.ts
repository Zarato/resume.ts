import { ConfigModel } from '../model';
import * as fs from 'fs';
import * as toml from 'toml-patch';

export async function fromToml(filename: string): Promise<ConfigModel> {
    const file = await fs.promises.readFile(filename, { encoding: 'utf-8' });
    return <ConfigModel>toml.parse(file);
}
import { fromJson } from './json';
import { fromToml } from './toml';
import { fromYaml } from './yaml';
import { ConfigModel } from '../model';
import * as path from 'path';

export const readers = {
    json: fromJson,
    toml: fromToml,
    yaml: fromYaml
};

export function fromName(name: string): (filename: string) => Promise<ConfigModel> {
    if (readers.hasOwnProperty(name)) {
        return readers[name as keyof typeof readers];
    }
    throw new Error(`Reader ${name} does not exist.`);
}

export async function read(filename: string): Promise<ConfigModel> {
    const extension = path.extname(filename).replace('.', '');
    const reader = fromName(extension);
    return await reader(filename);
}

export { fromJson };
export { fromToml };
export { fromYaml };
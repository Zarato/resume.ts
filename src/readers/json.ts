import { ConfigModel } from '../model';
import * as fs from 'fs';

export async function fromJson(filename: string): Promise<ConfigModel> {
    const file = await fs.promises.readFile(filename, { encoding: 'utf-8' });
    return <ConfigModel>JSON.parse(file);
}
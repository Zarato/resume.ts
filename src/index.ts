import commander from 'commander';
import prompts from 'prompts';
import ora from 'ora';
import * as path from 'path';
import * as fs from 'fs';
import * as writers from './writers/index';
import * as compilers from './compilers/index';
import * as readers from './readers/index';
import render from './render';
const program = new commander.Command();

const newCommand = program.command('new');
newCommand
    .command('template')
    .description('create a new template')
    .action(async () => {
        let { name, include, output } = await prompts([
            {
                type: 'text',
                name: 'name',
                message: 'What is the name of your template ?'
            },
            {
                type: 'confirm',
                name: 'include',
                message: 'Do you want to include default header ?',
                initial: true
            },
            {
                type: 'text',
                name: 'output',
                message: 'Where do you want to save ?',
                initial: './templates'
            }
        ]);

        const spinner = ora('Creating template file').start();

        const header = "<%- include('header'); -%>";
        const body = `
\\documentclass{article}

\\begin{document}

\\end{document}
        `;

        const fileContent = include ? header + body : body;

        if (output.startsWith('.')) {
            output = path.join(__dirname, '..', output); // writes in src/ folder if i don't specify '..'
        }

        const dirName = output;
        const fileName = path.resolve(dirName, `${name}.tex`);
        try {
            if (!fs.existsSync(dirName)) {
                await fs.promises.mkdir(dirName, { recursive: true });
            }
            await fs.promises.writeFile(fileName, fileContent);
            spinner.succeed(`File written at ${fileName}`);
        } catch (e) {
            spinner.fail(`${e}`);
        }
    });

newCommand
    .command('resume')
    .description('create a new resume')
    .action(async () => {
        let { extension, name, compiler, email, phone, resumeName, output, template } = await prompts([
            {
                type: 'select',
                name: 'extension',
                message: 'Choose an extension for the config file',
                choices: Object.keys(writers.writers).map(key => {
                    return {
                        title: key,
                        value: key
                    }
                }),
                initial: 1
            },
            {
                type: 'text',
                name: 'name',
                message: 'What is your name ?'
            },
            {
                type: 'select',
                name: 'compiler',
                message: 'Choose a compiler',
                choices: Object.keys(compilers.compilers).map(key => {
                    return {
                        title: key,
                        value: key
                    }
                }),
                initial: 0
            },
            {
                type: 'text',
                name: 'email',
                message: 'What is your email ?'
            },
            {
                type: 'text',
                name: 'phone',
                message: 'What is your phone number ?'
            },
            {
                type: 'text',
                name: 'resumeName',
                message: 'What is the name of your resume ?'
            },
            {
                type: 'text',
                name: 'output',
                message: 'Where do you want to save the config file ? (directory)'
            },
            {
                type: 'text',
                name: 'template',
                message: 'Which template do you want to use ? (full path)'
            }
        ]);

        if (output.startsWith('.')) {
            output = path.join(__dirname, '..', output);
        }

        if (template.startsWith('.')) {
            template = path.join(__dirname, '..', template);
        }

        const spinner = ora('Creating config file').start();
        const outputConfig = path.join(output, `${resumeName}.${extension}`);
        const model = {
            template: template,
            output: output,
            compiler: compiler,
            informations: {
                name: name,
                contact: {
                    email: email,
                    phone: phone
                }
            }
        };
        try {
            const writer = writers.fromName(extension);
            const data = writer(model);

            if (!fs.existsSync(output)) {
                await fs.promises.mkdir(output, { recursive: true });
            }
            await fs.promises.writeFile(outputConfig, data);
            spinner.succeed(`File written at ${outputConfig}`);
        } catch (e) {
            spinner.fail(`${e}`);
        }
    });

program
    .command('compile <source>')
    .description('compile a TeX file from config file')
    .action(async source => {
        let spinner = ora('Reading file').start();
        const model = await readers.read(path.resolve(source));
        spinner.succeed('File read');

        spinner = ora('Checking version').start();
        const compiler = compilers.fromName(model.compiler || 'pdflatex');
        try {
            spinner.succeed(`Using ${model.compiler} with the version ${await compiler.version()}`);

            spinner = ora('Rendering').start();
            const data = await render(model.template, model);
            await fs.promises.writeFile(source.replace(path.extname(source), '-ejs.tex'), data);
            spinner.succeed('Template rendered');

            spinner = ora('Compiling').start();
            const args: string[] = model.args || [];
            const saved = await compiler.compile(source.replace(path.extname(source), '-ejs.tex'), model.output, ...args);
            spinner.succeed(`File saved to ${saved}`);

            spinner = ora('Cleaning files').start();
            await compiler.clean(source.replace(path.extname(source), '-ejs.tex'));
            spinner.succeed('Files removed');

        } catch (e) {
            spinner.fail(`${e}`);
        }
    });

program.parse(process.argv);
import { Compiler } from './compiler';
import { execSync, exec } from 'child_process';
import fg from 'fast-glob';
import * as path from 'path';
import * as fs from 'fs';
import { promisify } from 'util';

const promiseExec = promisify(exec);

export class PdfLatexCompiler implements Compiler {

    version(): string | null {
        const output: string = execSync('pdflatex --version', { encoding: 'utf-8' });
        const lines = output.split('\n');
        const firstLine = lines[0];
        if (firstLine.indexOf('pdfTeX') === -1) {
            return null;
        }
        const version = lines[0].split(' ')[1];
        return version;
    }

    installed(): boolean {
        return this.version() !== null;
    }

    async compile(filepath: string, outputDir?: string, ...args: string[]): Promise<string> {
        outputDir = outputDir || path.dirname(filepath);
        const { stdout, stderr } = await promiseExec(['pdflatex', '-halt-on-error', `-output-directory=${outputDir}`, filepath, ...args].join(' '));
        if (stderr) {
            throw new Error(stderr);
        }

        const output = stdout;
        const regex = /Output written on (.\D+) \(/gm;
        const matches = regex.exec(output);
        if (matches === null) {
            throw new Error('Error during compilation');
        }
        const file = matches[1].replace('\n', '');

        return file;
    }

    async clean(filename: string) {
        const extensions = ['.aux', '.log'];
        const filepath = path.dirname(filename);
        const filters = extensions.map(extension => path.join(filepath, `*${extension}`));

        const files = await fg(filters);

        files.forEach(file => {
            fs.unlinkSync(file);
        })
    }
}
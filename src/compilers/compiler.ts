export interface Compiler {
    version(): string | null;

    installed(): boolean;

    compile(filepath: string, outputDir?: string, ...args: string[]): Promise<string>;

    clean(path: string): any;
}
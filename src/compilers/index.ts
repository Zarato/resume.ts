import { Compiler } from './compiler';
import { PdfLatexCompiler } from './pdflatex';

export { Compiler };
export { PdfLatexCompiler };

export const compilers = {
    pdflatex: PdfLatexCompiler,
};

export function fromName(name: string): Compiler {
    if (compilers.hasOwnProperty(name)) {
        return new compilers[name as keyof typeof compilers];
    }
    throw new Error(`Compiler ${name} does not exist.`);
};
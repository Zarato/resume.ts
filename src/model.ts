export interface ConfigModel {
    template: string;
    output?: string;
    args?: string[];
    compiler?: string;
    informations?: InformationsModel;
    objective?: string;
    education?: SchoolModel[];
    experience?: ExperienceModel[];
    activities?: ActivityModel[];
    skills?: SkillModel[];
    params?: object;
}

export interface InformationsModel {
    name?: string;
    address?: string[];
    contact?: ContactModel;
    websites?: WebsiteModel[];
}

export interface ContactModel {
    email?: string;
    phone?: string;
}

export interface WebsiteModel {
    text?: string;
    url?: string;
}

export interface SchoolModel {
    school?: string;
    startdate?: string;
    enddate?: string;
    degrees?: string[];
    achievements?: string[];
}

export interface ExperienceModel {
    company?: string;
    titles?: TitleModel[];
    projects?: string[];
}

export interface TitleModel {
    name?: string;
    startdate?: string;
    enddate?: string;
}

export interface ActivityModel {
    name?: string;
    startdate?: string;
    enddate?: string;
}

export interface SkillModel {
    category?: string;
    skills?: string[];
}
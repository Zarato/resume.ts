import { ConfigModel } from '../model';
import * as fs from 'fs';
import * as toml from 'toml-patch';

export function toToml(data: ConfigModel): string {
    return toml.stringify(data);
}
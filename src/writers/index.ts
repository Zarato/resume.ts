import { toJson } from './json';
import { toToml } from './toml';
import { toYaml } from './yaml';
import { ConfigModel } from '../model';
import * as path from 'path';

export const writers = {
    json: toJson,
    toml: toToml,
    yaml: toYaml
};

export function fromName(name: string): (data: ConfigModel) => string {
    if (writers.hasOwnProperty(name)) {
        return writers[name as keyof typeof writers];
    }
    throw new Error(`writer ${name} does not exist.`);
}

export function write(data: ConfigModel, name: string): string {
    const writer = fromName(name);
    return writer(data);
}

export { toJson };
export { toToml };
export { toYaml };
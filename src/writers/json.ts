import { ConfigModel } from '../model';

export function toJson(data: ConfigModel): string {
    return JSON.stringify(data);
}
import { ConfigModel } from '../model';
import * as fs from 'fs';
import YAML from 'yaml';

export function toYaml(data: ConfigModel): string {
    return YAML.stringify(data);
}
import * as ejs from 'ejs';
import { ConfigModel } from './model';

export default async function (template: string, model: ConfigModel): Promise<string> {
    return await ejs.renderFile(template, model);
}
import { fromJson } from '../../src/readers/index';
import { ConfigModel } from '../../src/model';
import * as fs from 'fs';
import * as path from 'path';

const model: ConfigModel = {
    template: 'test.tex',
    output: 'test.pdf',
    args: ['-test'],
    compiler: 'pdflatex',
    informations: {
        name: 'Zarato',
        address: ['gitlab'],
        contact: {
            email: 'ahah',
            phone: '00'
        },
        websites: [
            {
                text: 'personal blog',
                url: 'https://zarato.gitlab.io/zarato-blog'
            }
        ]
    },
    objective: 'Be a researcher',
    education: [
        {
            school: '??',
            startdate: '09-2018'
        }
    ],
    skills: [
        {
            category: 'Physics',
            skills: [
                'Mechanics',
                'Thermodynamics'
            ]
        },
        {
            category: 'Maths',
            skills: [
                'Algebra'
            ]
        }
    ],
    params: {
        ProjectEuler: 'Zarato'
    }
};

const file = 'test.json';

describe('Test JSONReader', () => {
    test('should parse JSON', async () => {
        const config = await fromJson(path.resolve(__dirname, file));
        expect(config).toEqual(model);
    });
});
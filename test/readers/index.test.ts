import * as reader from '../../src/readers/index';

describe('Test readers', () => {
    test('should return reader', () => {
        let func = reader.fromName('json');
        expect(func).toBe(reader.fromJson);

        func = reader.fromName('toml');
        expect(func).toBe(reader.fromToml);

        func = reader.fromName('yaml');
        expect(func).toBe(reader.fromYaml);

        expect(() => reader.fromName('bad')).toThrow();
    });
});
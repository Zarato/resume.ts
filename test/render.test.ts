import render from '../src/render';
import * as path from 'path';

describe('Test EJS rendering', () => {
    test('should render', async () => {
        const template = path.resolve(__dirname, 'test.ejs');
        const model = {
            output: 'here',
            args: [
                '-a',
                '-b'
            ]
        };
        await expect(render(template, model)).resolves.toBe('Output: here, Args: -a, -b');
    })
})
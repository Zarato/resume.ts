import * as pdflatex from '../../src/compilers/index';
import * as fs from 'fs';
import * as path from 'path';
import fg from 'fast-glob';

describe('Test PdfLatexCompiler', () => {
    const compiler = new pdflatex.PdfLatexCompiler();

    test('should return version', () => {
        expect(compiler.version()).toBe('3.14159265-2.6-1.40.20');
    });

    test('should check if installed', () => {
        expect(compiler.installed()).toBe(true);
    });

    test('should compile', async () => {
        const file = 'good.tex';
        const filename = path.resolve(__dirname, file);
        const data = await compiler.compile(filename);
        expect(data).toBe(filename.replace('.tex', '.pdf'));
    });

    test('should not compile', async () => {
        const file = 'bad.tex';
        await expect(compiler.compile(path.resolve(__dirname, file))).rejects.toThrow();
    });

    test('should clean files', async () => {
        const file = 'good.tex';
        await compiler.clean(path.resolve(__dirname, file));

        const extensions = ['.aux', '.log'];
        const filepath = __dirname;
        const filters = extensions.map(extension => path.join(filepath, `*${extension}`));

        const files = await fg(filters);
        expect(files.length).toEqual(0);
    })
});